package.path = './?.lua;./libraries/?.lua;./libraries/?/init.lua'

local sock = require('sock')
local bitser = require('bitser')
local array = require('array')
local setup = require('setupBoard')
local Pieces = {
    Bishop = require('class/piece/bishop'),
    King = require('class/piece/king'),
    Knight = require('class/piece/knight'),
    Pawn = require('class/piece/pawn'),
    Queen = require('class/piece/queen'),
    Rook = require('class/piece/rook'),
}

server = sock.newServer("*", 22122, 2)
board = array:new(8, 8)
activePlayer = 1

function getCoord(x, y)
    alpha = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'}
    return alpha[x]..y
end

server:on("connect", function(data, client)
    print("Player "..client:getIndex().." connected")

    local pieces = {}
    for i, x, y, item in board:apairs() do
        table.insert(pieces, {i, item.class.name, item.color})
    end

    client:send("playerNum", client:getIndex())
    client:send("activePlayer", activePlayer)
    client:send("setupBoard", pieces)
end)

server:on("move", function(data, client)
    if client:getIndex() == activePlayer then
        x1, y1, x2, y2 = unpack(data)
        print("Player "..client:getIndex().." moved "..getCoord(x1, y1).." to "..getCoord(x2, y2))
        local piece = board:get(x1, y1)
        if piece:validMove(x1, y1, x2, y2) then
            board:set(x1, y1, nil)
            board:set(x2, y2, piece)

            for _, client in ipairs(server:getClients()) do
                client:send("move", {x1, y1, x2, y2})
            end
        end
        activePlayer = (activePlayer%2)+1
        for _, c in ipairs(server:getClients()) do
            c:send("activePlayer", activePlayer)
        end
    else
        print("Player "..client:getIndex().." is trying to take an extra turn")
    end
end)

setup(array)

while true do
    server:update()
end
