local class = require('middleclass')
local Piece = require('class/piece')

local Bishop = class('Bishop', Piece)

function Bishop:initialize(color, array)
	Piece.initialize(self, color, array)

	if love then
		if color == 1 then
			self.sprite = love.graphics.newImage('resource/whiteBishop.png')
		elseif color == 2 then
			self.sprite = love.graphics.newImage('resource/blackBishop.png')
		end
	end
end

function Bishop:validMove(x1, y1, x2, y2)
	local destCell = self.array:get(x2, y2)
	local xDistance = math.abs(x1 - x2)
	local yDistance = math.abs(y1 - y2)

	if xDistance == yDistance then
		for i=1, xDistance-1 do
			local x, y = zMath.toward(x1, x2, i), zMath.toward(y1, y2, i)
			local cell = self.array:get(x, y)
			if cell ~= nil then
				return false
			end
		end
		if not self:isFriendly(self.array:get(x2, y2)) then
			return true
		end
	end
end

return Bishop
