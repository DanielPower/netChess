local class = require('middleclass')
local Piece = require('class/piece')

local Pawn = class('Pawn', Piece)

function Pawn:initialize(color, array)
	Piece.initialize(self, color, array)

	self.moved = false

	if love then
		if color == 1 then
			self.sprite = love.graphics.newImage('resource/whitePawn.png')
		elseif color == 2 then
			self.sprite = love.graphics.newImage('resource/blackPawn.png')
		end
	end
end

function Pawn:validMove(x1, y1, x2, y2)
	local destCell = self.array:get(x2, y2)

	-- Pawn can only move one space (or two, but only for the first move)
	local distance
	if self.color == 1 then
		distance = y1 - y2
	elseif self.color == 2 then
		distance = y2 - y1
	end

	-- Pawn can only move forward (unless attacking)
	if x1 == x2 then
		if self.moved == false then
			if distance == 1 or distance == 2 then
				if self.array:get(x2, y2) == nil then
					return(true)
				end
			end
		elseif self.moved == true then
			if distance == 1 then
				if self.array:get(x2, y2) == nil then
					return(true)
				end
			end
		end
	-- Pawn can move one space diagonally to attack
	elseif math.abs(x1 - x2) == 1 and distance == 1 then
		if destCell and destCell.color ~= self.color then
			return(true)
		end
	end
end

return Pawn
