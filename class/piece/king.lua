local class = require('middleclass')
local Piece = require('class/piece')

local King = class('King', Piece)

function King:initialize(color, grid)
	Piece.initialize(self, color, grid)

	if love then
		if color == 1 then
			self.sprite = love.graphics.newImage('resource/whiteKing.png')
		elseif color == 2 then
			self.sprite = love.graphics.newImage('resource/blackKing.png')
		end
	end
end

function King:validMove(x1, y1, x2, y2)
	local destination = self.grid:check(x2, y2)
	local xDistance = math.abs(x1 - x2)
	local yDistance = math.abs(y1 - y2)

	if xDistance > 0 or yDistance > 0 then
		if xDistance <= 1 and yDistance <= 1 then
			if not self:isFriendly(self.grid:check(x2, y2)) then
				return true
			end
		end
	end
end

return King
