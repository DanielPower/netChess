local class = require('middleclass')
local Piece = require('class/piece')

local Queen = class('Queen', Piece)

function Queen:initialize(color, grid)
	Piece.initialize(self, color, grid)

	if love then
		if color == 1 then
			self.sprite = love.graphics.newImage('resource/whiteQueen.png')
		elseif color == 2 then
			self.sprite = love.graphics.newImage('resource/blackQueen.png')
		end
	end
end

function Queen:validMove(x1, y1, x2, y2)
	local destination = self.grid:check(x2, y2)
	local xDistance = math.abs(x1 - x2)
	local yDistance = math.abs(y1 - y2)

	-- Horizontal
	if xDistance > 0 and yDistance == 0 then
		for x in range( zMath.toward(x1, x2), x2 ) do
			local cell = self.grid:check(x, y1)
			if cell ~= nil and cell ~= destination then
				return false
			end
		end
		if not self:isFriendly(self.grid:check(x2, y2)) then
			return true
		end
	end

	-- Vertical
	if yDistance > 0 and xDistance == 0 then
		for y in range( zMath.toward(y1, y2), y2 ) do
			local cell = self.grid:check(x1, y)
			if cell ~= nil and cell ~= destination then
				return false
			end
		end
		if not self:isFriendly(self.grid:check(x2, y2)) then
			return true
		end
	end

	-- Diagonal
	if xDistance == yDistance then
		for i=1, xDistance-1 do
			local x, y = zMath.toward(x1, x2, i), zMath.toward(y1, y2, i)
			local cell = self.grid:check(x, y)
			if cell ~= nil then
				return false
			end
		end
		if not self:isFriendly(self.grid:check(x2, y2)) then
			return true
		end
	end
end

return Queen
