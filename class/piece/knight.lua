local class = require('middleclass')
local Piece = require('class/piece')

local Knight = class('Knight', Piece)

function Knight:initialize(color, grid)
	Piece.initialize(self, color, grid)

	if love then
		if color == 1 then
			self.sprite = love.graphics.newImage('resource/whiteKnight.png')
		elseif color == 2 then
			self.sprite = love.graphics.newImage('resource/blackKnight.png')
		end
	end
end

function Knight:validMove(x1, y1, x2, y2)
	local destCell = self.grid:check(x2, y2)
	local xDistance = math.abs(x1 - x2)
	local yDistance = math.abs(y1 - y2)

	if xDistance == 2 and yDistance == 1 then
		if destCell ~= nil then
			if destCell.color == self.color then
				return(false)
			end
		end
		return(true)
	elseif xDistance == 1 and yDistance == 2 then
		if destCell ~= nil then
			if destCell.color == self.color then
				return(false)
			end
		end
		return(true)
	end
end

return Knight
