local class = require('middleclass')

local Piece = class('Piece')
Piece.rules = {}
Piece.selected = nil

function Piece:initialize(color, array)
	self.array = array
	self.color = color
end

function Piece:isFriendly(piece)
	if piece then
		if piece.color == self.color then
			return true
		else
			return false
		end
	end
end

function Piece:draw(x, y, r)
	if selection.piece == self then
		love.graphics.setColor(0.176, 0.784, 0.176, 0.5)
		love.graphics.rectangle('fill', x, y, r, r)
		love.graphics.setColor(0.176, 0.176, 0.784, 0.5)

		local _, x1, y1 = self.array:find(self)
		for x2=1, 8 do
			for y2=1, 8 do
				if self:validMove(x1, y1, x2, y2) then
					love.graphics.rectangle('fill', (x2-1)*r, (y2-1)*r, r, r)
				end
			end
		end
	end
	love.graphics.setColor(1, 1, 1, 1)
	love.graphics.draw(self.sprite, x, y)
end

return Piece
