package.path = './?.lua;./libraries/?.lua;./libraries/?/init.lua'

local sock = require('sock')
local array = require('array')
local Pieces = {
    Bishop = require('class/piece/bishop'),
    King = require('class/piece/king'),
    Knight = require('class/piece/knight'),
    Pawn = require('class/piece/pawn'),
    Queen = require('class/piece/queen'),
    Rook = require('class/piece/rook'),
}

local background = love.graphics.newImage('resource/board.png')
local board = array:new(8, 8)
local playerNum = nil
local activePlayer = nil

selection = {}
cellSize = 96

function love.load()
    -- Connect to server
    client = sock.newClient('localhost', 22122)

    client:on("connect", function(data)
        print("Client connected to the server")
    end)

    client:on("playerNum", function(num)
        playerNum = num
    end)

    client:on("setupBoard", function(data)
        for _, item in ipairs(data) do
            board.cells[item[1]] = Pieces[item[2]]:new(item[3], board)
        end
    end)

    client:on("move", function(data)
        local x1, y1, x2, y2, activePlayer = unpack(data)
        local piece = board:get(x1, y1)
        board:set(x1, y1, nil)
        board:set(x2, y2, piece)
    end)

    client:on("activePlayer", function(player)
        activePlayer = player
        print("activePlayer is now ".. player)
    end)

    client:connect()
end

function love.mousepressed(x, y, button)
    print(playerNum, activePlayer)
    x = math.ceil(x/cellSize)
    y = math.ceil(y/cellSize)

    local piece = board:get(x, y)
    if activePlayer == playerNum then
        if not selection.piece then
            if piece and (playerNum == piece.color) then
                selection = {x = x, y = y, piece = piece}
            end
        else
            x1, y1, x2, y2 = selection.x, selection.y, x, y
            if selection.piece:validMove(x1, y1, x2, y2) then
                client:send('move', {x1, y1, x2, y2})
            end
            selection = {}
        end
    end
end

function love.update(dt)
    client:update()
end

function love.draw()
    love.graphics.draw(background)
    for _, x, y, item in board:apairs() do
        item:draw((x-1)*96, (y-1)*96, cellSize)
    end
end
