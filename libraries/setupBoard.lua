local Pawn = require('class/piece/pawn')
local Rook = require('class/piece/rook')
local Knight = require('class/piece/knight')
local Queen = require('class/piece/queen')
local King = require('class/piece/king')
local Bishop = require('class/piece/bishop')

return(
	function(array)
		-- Place pawns
		for i=1, 8 do
			array:set(i, 7, Pawn:new(1, array))
			array:set(i, 2, Pawn:new(2, array))
		end

		-- Place rooks
		for i=1, 8, 7 do
			array:set(i, 8, Rook:new(1, array))
			array:set(i, 1, Rook:new(2, array))
		end

		-- Place knights
		for i=2, 7, 5 do
			array:set(i, 8, Knight:new(1, array))
			array:set(i, 1, Knight:new(2, array))
		end

		-- Place Queens
		array:set(4, 8, Queen:new(1, array))
		array:set(4, 1, Queen:new(2, array))

		-- Place Kings
		array:set(5, 8, King:new(1, array))
		array:set(5, 1, King:new(2, array))

		-- Place bishops
		for i=3, 6, 3 do
			array:set(i, 8, Bishop:new(1, array))
			array:set(i, 1, Bishop:new(2, array))
		end
	end
)
